using Xunit;

namespace PeselValidator.Tests
{
    public class PeselValidatorTests
    {
        [Fact]
        public void PeselValidatorTest()
        {
            var testDate = new Core.PeselValidation.PeselValidator();
            
            Assert.True(testDate.IsPeselValid("44051401458"));
            Assert.False(testDate.IsPeselValid("40001401358"));
        }
    }
}