namespace PeselValidator.Core.DateValidation
{
    public class YearValidator
    {
        private const int YearLimit = 0;

        public static bool ValidateYear(int year)
        {
            return year >= YearLimit;
        }

        public static bool IsLeapYear(int year)
        {
            return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
        } 

    }
}