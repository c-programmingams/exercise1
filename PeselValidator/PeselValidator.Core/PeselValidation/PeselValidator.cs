using System;
using System.Linq;
using PeselValidator.Core.DateValidation;

namespace PeselValidator.Core.PeselValidation
{
    public class PeselValidator: IPeselValidator
    {
        protected const int PeselLength = 11;
        protected const int DateLength = 6;
        protected const int DayLength = 4;
        protected const int MonthLength = 2;
        protected const int YearLength = 0;
        private const int SubLength = 2;

        private int _year;
        private int _month;
        private int _day;

        private readonly int[] _controlNums = new int[] { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 0 };
        
        public string CutOffDate(string pesel)
        {
            return pesel.Length >= DateLength ? pesel.Remove(DateLength) : "ERROR - PESEL TOO SHORT";
        }

        public bool Checksum(string pesel)
        {
            var peselArray = pesel.ToCharArray();
            if (peselArray.Length != PeselLength)
                return false;

            int suma = peselArray.Select((t, i) => Int32.Parse(t.ToString()) * _controlNums[i]).Sum();
            return (10 - suma % 10) == Int32.Parse(peselArray.LastOrDefault().ToString());
        }

        public bool ValidateDate(int year, int month, int day)
        {
            return YearValidator.ValidateYear(year) && MonthValidator.ValidateMonth(month) &&
                   DayValidator.ValidateDay(year, month, day);
        }

        public bool IsPeselValid(string pesel)
        {
            return GetDate(CutOffDate(pesel)) && ValidateDate(_year, _month, _day) && Checksum(pesel);
        }
        
        private bool GetDate(string date)
        {
            try
            {
                _year = Int32.Parse(date.Substring(YearLength, SubLength));
                _month = Int32.Parse(date.Substring(MonthLength, SubLength));
                _day = Int32.Parse(date.Substring(DayLength, SubLength));
            }
            catch (Exception)
            {
                return false;
            }

            CenturyMonthMatcher monthAndYear = new CenturyMonthMatcher();
            var list = monthAndYear.GetMonthYearList(_month, _year).ToArray();

            _month = list[0];
            _year = list[1];

            return true;
        }

        public static bool IsDateValid(int year, int month, int day)
        {
            return YearValidator.ValidateYear(year) && MonthValidator.ValidateMonth(month) &&
                   DayValidator.ValidateDay(year, month, day);
        }

    }
}