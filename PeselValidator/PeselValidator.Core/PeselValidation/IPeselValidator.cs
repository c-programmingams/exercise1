namespace PeselValidator.Core.PeselValidation
{
    public interface IPeselValidator
    {
        string CutOffDate(string pesel);
        bool Checksum(string pesel);
        bool ValidateDate(int year, int month, int day);
        bool IsPeselValid(string pesel);
    }
}