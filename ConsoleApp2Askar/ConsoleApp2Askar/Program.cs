﻿using System;
using System.Collections.Generic;

namespace ConsoleApp2Askar
{
    class Program
    {
        private const int February = 2;
        private const int MinDayLimit = 1;
        private static int _month;
        private static int _year;
        
        private static Dictionary<int, int> _monthDay = new Dictionary<int, int>()
        {
            [1]= 31,
            [3]= 31,
            [4]= 30,
            [5]= 31,
            [6]= 30,
            [7]= 31,
            [8]= 31,
            [9]= 30,
            [10]= 31,
            [11]= 30,
            [12]= 31,
        };

        public static bool IsLeapYear(int year)
        {
            return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
        }
        
        private static int MaxDayLimit()
        {
            if (_month == February)
                return IsLeapYear(_year) ? 29 : 28;

            if (_monthDay.ContainsKey(_month))
                return _monthDay[_month];

            return 0;
        }
        
        public static bool ValidateDay3(int year, int month, int day)
        {
            _month = month;
            _year = year;

            return (day >= MinDayLimit && day <= MaxDayLimit());
        } 
        
        static void Main(string[] args)
        {
            int year = 2019;
            int month = 12;
            int day = -13;
            _monthDay.Add();
            
            var result_1 = ValidateDay1(year, month, day);
            var result_3 = ValidateDay3(year, month, day);
            
        }
        
        
        if (month == 1)
        {
            return day >= MinDayLimit && day <= 31;
        }
        
        
        
        if (month == 1)
        {
            
        }
        else
        {
            return true;
        }
        
        public static bool ValidateDay1(int year, int month, int day)
        {
            if (month == 1)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 2)
            {
                
            }
            
            
            
            
            
            else if(month == 3)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 4)
            {
                return day >= MinDayLimit && day <= 30;
            }
            else if(month == 5)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 6)
            {
                return day >= MinDayLimit && day <= 30;
            }
            else if(month == 7)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 8)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 9)
            {
                return day >= MinDayLimit && day <= 30;
            }
            else if(month == 10)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 11)
            {
                return day >= MinDayLimit && day <= 30;
            }
            else if(month == 12)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 13)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else
            {
                return false;
            }
        }
        
        public static bool ValidateDay2(int year, int month, int day)
        {
            switch (month)
            {
                case 1:
                    return day >= MinDayLimit && day <= 31;
                case 2:
                    return day >= MinDayLimit && day <= 31;
                case 3:
                    return day >= MinDayLimit && day <= 31;
                case 4:
                    return day >= MinDayLimit && day <= 31;
                case 5:
                    return day >= MinDayLimit && day <= 31;
                case 6:
                    return day >= MinDayLimit && day <= 31;
                case 7:
                    return day >= MinDayLimit && day <= 31;
                case 8:
                    return day >= MinDayLimit && day <= 31;
                case 9:
                    return day >= MinDayLimit && day <= 31;
            }
            if (month == 1)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 2)
            {
                return day >= MinDayLimit && (IsLeapYear(year)? day<=29 : day<=28);
            }
            else if(month == 3)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 4)
            {
                return day >= MinDayLimit && day <= 30;
            }
            else if(month == 5)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 6)
            {
                return day >= MinDayLimit && day <= 30;
            }
            else if(month == 7)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 8)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 9)
            {
                return day >= MinDayLimit && day <= 30;
            }
            else if(month == 10)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 11)
            {
                return day >= MinDayLimit && day <= 30;
            }
            else if(month == 12)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else if(month == 13)
            {
                return day >= MinDayLimit && day <= 31;
            }
            else
            {
                return false;
            }
        }
    }
}