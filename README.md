# exercise1

# zadanie_2

C# from scratch
1. [Anonymous Type](https://www.tutorialsteacher.com/csharp/csharp-anonymous-type)
2. [Dynamic Type](https://www.tutorialsteacher.com/csharp/csharp-dynamic-type)
3. [Enum](https://www.tutorialsteacher.com/csharp/csharp-enum)
4. [Tuple](https://www.tutorialsteacher.com/csharp/csharp-tuple)
5. [Generics](https://www.tutorialsteacher.com/csharp/csharp-generics)
[Generics_MS](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/generics/constraints-on-type-parameters)
6. [Generic Collections](https://www.tutorialsteacher.com/csharp/csharp-generic-collections)
7. [Exception](https://www.tutorialsteacher.com/csharp/csharp-exception)
8. [Delegates](https://www.tutorialsteacher.com/csharp/csharp-delegates)
9. [Anonymous Methods](https://www.tutorialsteacher.com/csharp/csharp-anonymous-method)
10. [Func](https://www.tutorialsteacher.com/csharp/csharp-func-delegate)
11. [Action](https://www.tutorialsteacher.com/csharp/csharp-action-delegate)
12. [Predicate](https://www.tutorialsteacher.com/csharp/csharp-predicate)
13. [Nullable Types](https://www.tutorialsteacher.com/csharp/csharp-nullable-types)
14. [Covariance and Contravariance](https://www.tutorialsteacher.com/csharp/csharp-covariance-and-contravariance)
15. [LINQ, Lamda Expressions, Expression, Expression Tree](https://www.tutorialsteacher.com/linq/linq-tutorials)
16. [IEnumerable vs ICollection vs IList vs IQueryable](https://medium.com/@kunaltandon.kt/ienumerable-vs-icollection-vs-ilist-vs-iqueryable-in-c-2101351453db)
17. [Fluent API / Fluent interface](https://en.wikipedia.org/wiki/Fluent_interface#C#)
18. [Extension Methods](https://www.tutorialsteacher.com/csharp/csharp-extension-method)
19. [Object Initializer](https://www.tutorialsteacher.com/csharp/csharp-object-initializer)
20. [Task Parallel Library (TPL)](https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/task-parallel-library-tpl)
21. [Reflection](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/reflection)


