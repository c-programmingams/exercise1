﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TPL
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new List<int> {1, 2, 3, 4, 5, 6, 7, 8};
            Parallel.ForEach(
                list,
                new ParallelOptions { MaxDegreeOfParallelism = 2 },
                x => { Console.WriteLine(x); }
            );

        }
    }
}
