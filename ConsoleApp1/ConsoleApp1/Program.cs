﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    public class Program5
    {
        public int sProperty { get; set; }
        public string sProperty2 { get; set; }
        public string sProperty23 { get; set; }
        public string sProperty34 { get; set; }
        public string sProperty5 { get; set; }
        public int Name { get; set; }


        public void Main2(string args)
        {
            Console.WriteLine("Hello World!");
            
        }
    }

    public interface IProgram3
    {
        void Main2(string args);
    }
    public static class IntExtensions
    {
        public static bool IsGreaterThan(this int i, int value)
        {
            return i > value;
        }
    }

    public class Program
    {
        public static void Main()
        {
            int i = 1;
            var result = i.IsGreaterThan(2);
            IList<Student> studentList = new List<Student>() { 
                new Student() { StudentID = 1, StudentName = "John", Age = 13 } ,
                new Student() { StudentID = 2, StudentName = "Steve",  Age = 15 } ,
                new Student() { StudentID = 3, StudentName = "Bill",  Age = 18 } ,
                new Student() { StudentID = 4, StudentName = "Ram" , Age = 12 } ,
                new Student() { StudentID = 5, StudentName = "Ron" , Age = 21 } 
            };
            
            var teenAgerStudents = studentList.Where(s => s.Age > 12 && s.Age < 20);

            foreach (Student teenStudent in teenAgerStudents)
                Console.WriteLine("Student Name: {0}", teenStudent.StudentName);
            
            Console.WriteLine("Second run");
            
            studentList.Add(new Student {StudentID = 6, StudentName ="Martin", Age = 16});
            
            foreach (Student teenStudent in teenAgerStudents)
                Console.WriteLine("Student Name: {0}", teenStudent.StudentName);
        }
    }

    public class Student{

        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }
    }
    
}